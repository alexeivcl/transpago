package com.jtc.transpago.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.jtc.transpago.bean.Cuenta;
import com.jtc.transpago.bean.Movimiento;
import com.jtc.transpago.bean.Transferencia;

@Service
public class MovimientoServiceImpl implements MovimientoService{
	
	private static final String REST_MOVIMIENTO_URL = "http://localhost:8095/rest/movimientos/";
	private static final String REST_MES_URL = "/m/";
	private static final String REST_CUENTA_URL = "/c/";
	private static final String REST_ANNO_URL = "/a/";
	private static final String REST_MONTO_URL = "/mnt/";
	private static final String REST_ANNOS_MOVIMIENTO_URL = "http://localhost:8095/rest/movimientos/annosFilter";
	private static final String REST_MONTO_VALIDO_URL = "http://localhost:8095/rest/movimientos/montoValido/";
	private static final String REST_AGREGAR_URL = "http://localhost:8095/rest/movimientos/agregar";

	private static List<Movimiento> movimientos;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public List<Movimiento> obtenerMovimientos(String nroDoc) {
		
		ResponseEntity<Movimiento[]> responseEntity = restTemplate.getForEntity(REST_MOVIMIENTO_URL + nroDoc, Movimiento[].class);
		movimientos = Arrays.asList(responseEntity.getBody());
		return movimientos;
	}

	@Override
	public List<Movimiento> obtenerMovimientosFiltros(String nroDoc, String nroCuenta, String mes, String anno) {

		ResponseEntity<Movimiento[]> responseEntity = restTemplate.getForEntity(REST_MOVIMIENTO_URL + nroDoc + REST_CUENTA_URL + nroCuenta + REST_MES_URL + mes + REST_ANNO_URL + anno, Movimiento[].class);
		movimientos = Arrays.asList(responseEntity.getBody());
		return movimientos;
	}
	
	@Override
	public List<String> obtenerAnnosMovimientos() {
		
		ResponseEntity<String[]> responseEntity = restTemplate.getForEntity(REST_ANNOS_MOVIMIENTO_URL, String[].class);
		List<String> annos = Arrays.asList(responseEntity.getBody());
		return annos;
	}

	@Override
	public Boolean obtenerMontoValido(String nroCuenta, Integer monto) {

		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(REST_MONTO_VALIDO_URL + REST_MONTO_VALIDO_URL + monto + REST_CUENTA_URL + nroCuenta,Boolean.class, monto);
		//return false;
	}
	
	@Override
	public Boolean insertarTranferencia(Transferencia transferencia) {
		
	    RestTemplate restTemplate = new RestTemplate();
	    return restTemplate.postForObject(REST_AGREGAR_URL, transferencia, Boolean.class);
	    	
		
	}
			
}
