package com.jtc.transpago.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.jtc.transpago.bean.Usuario;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	private static final String REST_USUARIO_URL = "http://localhost:8095/rest/usuario/";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public Boolean Login(String user, String Passw) {

		Usuario usuario = new Usuario();
		usuario.setNroDoc(user);
		usuario.setPin(Passw);
		
		UriComponentsBuilder ucb = UriComponentsBuilder.fromHttpUrl(REST_USUARIO_URL);
		ucb.path("login");
		
		Boolean response = restTemplate.postForObject(ucb.toUriString(), usuario, Boolean.class);		
		
		return response;
	}

	
}
