package com.jtc.transpago.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.jtc.transpago.bean.Cuenta;
import com.jtc.transpago.bean.Usuario;

@Service
public class CuentaServiceImpl implements CuentaService {

	private static final String REST_CUENTA_URL = "http://localhost:8095/rest/cuentas/";
	private static final String REST_CUENTA_DETAIL_URL = "http://localhost:8095/rest/cuentas/c/{nroCuenta}";
	private static final String REST_USUARIO_CUENTA_URL = "http://localhost:8095/rest/cuentas/uc/{nroCuenta}";
	//private static final String REST_EXISTE_CUENTA_URL = "http://localhost:8095/rest/cuentas/v:";

	private static List<Cuenta> cuentas;
	
	@Autowired
	private RestTemplate restTemplate;

	
	@Override
	public List<Cuenta> obtenerCuentasUsuario(String nroDoc) {
		
		ResponseEntity<Cuenta[]> responseEntity = restTemplate.getForEntity(REST_CUENTA_URL + nroDoc, Cuenta[].class);
		cuentas = Arrays.asList(responseEntity.getBody());
		return cuentas;
		
	}
	
	@Override
	public Cuenta obtenerDetallesCuenta(String nroCuenta) {
		
		Map<String, String> params = new HashMap<String, String>();
	    params.put("nroCuenta",nroCuenta);	      
	    
	    RestTemplate restTemplate = new RestTemplate();
	    Cuenta cuenta = restTemplate.getForObject(REST_CUENTA_DETAIL_URL, Cuenta.class, nroCuenta);		
		
		return cuenta;		
	}
	
	@Override
	public Usuario obtenerUsuarioCuenta(String nroCuenta) {
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("nroCuenta",nroCuenta);	      
		
		RestTemplate restTemplate = new RestTemplate();
		Usuario usuario = restTemplate.getForObject(REST_USUARIO_CUENTA_URL, Usuario.class, nroCuenta);		
		
		return usuario;		
	}

		

}
