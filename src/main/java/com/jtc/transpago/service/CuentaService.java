package com.jtc.transpago.service;

import java.util.List;

import com.jtc.transpago.bean.Cuenta;
import com.jtc.transpago.bean.Usuario;

public interface CuentaService {

	List<Cuenta> obtenerCuentasUsuario(String nroDoc);

	Cuenta obtenerDetallesCuenta(String nroCuenta);

	Usuario obtenerUsuarioCuenta(String nroCuenta);
}
