package com.jtc.transpago.service;

import java.util.List;

import com.jtc.transpago.bean.Movimiento;
import com.jtc.transpago.bean.Transferencia;

public interface MovimientoService {

	List<Movimiento> obtenerMovimientos(String nroDoc);
	List<Movimiento> obtenerMovimientosFiltros(String nroDoc, String nroCuenta, String mes, String anno);
	List<String> obtenerAnnosMovimientos();
	Boolean obtenerMontoValido(String nroCuenta, Integer monto);
	Boolean insertarTranferencia(Transferencia transferencia);

}
