package com.jtc.transpago.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

@Configuration
@EnableWebSecurity
public class WebConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private AuthenticationService authService;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
			.antMatchers("/home").permitAll()  // se especifica cuales son las url's que no necesitan ser autenticadas
			.anyRequest().authenticated()  //cualquier peticion diferente debe ser autenticada
		.and()
		.formLogin()
			.loginPage("/users/login").permitAll()  // formulario personalizado de login
			//.defaultSuccessUrl("/")
			//.failureUrl("/users/login?error")
			//.failureHandler(failure)
			.failureForwardUrl("/users/login")
			.usernameParameter("username")
			.passwordParameter("password")
			.defaultSuccessUrl("/autoservice")
			
		.and()
		.logout()//.logoutRequestMatcher(new AntPathRequestMatcher("/users/logout")).logoutSuccessUrl("/users/login")
		.permitAll()
		.and()
		.csrf().disable();
		
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		
		web
		.ignoring().antMatchers("/css/**", "/js/**");
		
	}

	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.authenticationProvider(authService);
	}	

}
