package com.jtc.transpago.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.jtc.transpago.service.UsuarioService;

@Component
public class AuthenticationService implements AuthenticationProvider {

	@Autowired
	private UsuarioService usuarioService;

	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {

		String usuario = auth.getName();
		String passw = (String) auth.getCredentials();
		
		
		Boolean logueado = usuarioService.Login(usuario, passw);

		if (logueado) {

			return new UsernamePasswordAuthenticationToken(usuario, passw, null); 
		}

		return null;
	}
	

	@Override
	public boolean supports(Class<?> auth) {
		
		return auth.equals(UsernamePasswordAuthenticationToken.class);
	}

}
