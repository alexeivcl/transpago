package com.jtc.transpago;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TranspagoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TranspagoApplication.class, args);
	}
}
