package com.jtc.transpago.controller.reporte;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jtc.transpago.bean.Movimiento;
import com.jtc.transpago.component.ReporteUtil;
import com.jtc.transpago.service.MovimientoService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

@Controller
@RequestMapping("/rest-report/")
public class ReporteController {

	@Autowired
	private MovimientoService movimientoService;
	
	@GetMapping("movimientos")
	@ResponseBody
	private void reporteMovimientos(HttpServletResponse response) {
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();

			List<Movimiento> movimientos = movimientoService.obtenerMovimientos(auth.getName());
			HashMap params = new HashMap<>();
			JasperPrint reporGenerated = ReporteUtil.generarReporte("reportes/ListaMovimientos.jrxml", movimientos, params);

			response.setContentType("application/x-pdf");
			response.setHeader("Content-disposition", "inline; filename=listaEmpleados.pdf");
			JasperExportManager.exportReportToPdfStream(reporGenerated, response.getOutputStream());

		} catch (Exception e) {
			e.printStackTrace(System.err);
		}

	}

}
