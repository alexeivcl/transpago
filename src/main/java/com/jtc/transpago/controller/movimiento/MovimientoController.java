package com.jtc.transpago.controller.movimiento;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jtc.transpago.bean.Cuenta;
import com.jtc.transpago.bean.Transferencia;
import com.jtc.transpago.bean.Usuario;
import com.jtc.transpago.service.CuentaService;
import com.jtc.transpago.service.MovimientoService;

@Controller
@RequestMapping("/movimientos")
public class MovimientoController {
	
	@Autowired
	private MovimientoService movimientoService;
	
	@Autowired
	private CuentaService cuentaService;
	
	//private String nroDoc = SecurityContextHolder.getContext().getAuthentication().getName();

	@GetMapping()
	public String obtenerEmpleados(Model model) { 
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String nroDoc = auth.getName();
		
		model.addAttribute("movimientos", movimientoService.obtenerMovimientos(nroDoc));
		model.addAttribute("cuentas", cuentaService.obtenerCuentasUsuario(nroDoc));
		model.addAttribute("annos", movimientoService.obtenerAnnosMovimientos());
		model.addAttribute("cuentaSelected", null);
		return "/movimientos/movimientos";
	}
	
	@GetMapping("/nuevo")
	public String nuevoMovimiento(Model model) { 
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String nroDoc = auth.getName();
		
		List<Cuenta> cuentas = cuentaService.obtenerCuentasUsuario(nroDoc);
		
//		cuentas = cuentas.stream().filter(x -> x.getTipo().equals("CUENTA CORRIENTE"))
//				.collect(Collectors.toList());
		
		model.addAttribute("cuentasDeb", cuentas);
		model.addAttribute("transferencia", new Transferencia());
		return "/movimientos/nuevo";
	}
	
	@PostMapping("/filter")
	public String filterMovimientos(Model model,HttpSession session,HttpServletRequest request) { 
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		String nroCuenta = (request.getParameter("cuenta") != "0") ? request.getParameter("cuenta") : "T";
		String mesMov = request.getParameter("mes");
		String movimiento = request.getParameter("anno");
		String nroDoc = auth.getName();
		
		model.addAttribute("movimientos", movimientoService.obtenerMovimientosFiltros(nroDoc, nroCuenta, mesMov, movimiento));
		model.addAttribute("cuentas", cuentaService.obtenerCuentasUsuario(nroDoc));
		model.addAttribute("cuentaSelected", (nroCuenta.equals("0")) ? null : cuentaService.obtenerDetallesCuenta(nroCuenta) );
		model.addAttribute("annos", movimientoService.obtenerAnnosMovimientos());
//		if (nroCuenta.equals("0")&&mesMov.equals("0")&&movimiento.equals("0")) {
//			obtenerEmpleados(model);
//		}
		
//		model.addAttribute("movimientos", movimientoService.obtenerMovimientos(nroDoc));			
		
		return "/movimientos/movimientos";
	}
	
	
	@ResponseBody
	@PostMapping("/validateNroCuenta")
    public Cuenta validateNroCuentaAjax(@RequestBody Cuenta cuenta) {

        Cuenta cuentaResult = cuentaService.obtenerDetallesCuenta(cuenta.getNroCuenta());
        
        return cuentaResult;
		
    }
	
//	@ResponseBody
//    @RequestMapping(value="/greeting",method = RequestMethod.GET)
//    public String Greeting(){
//        return "Message From SpringBoot Service - Hello World!";
//    }
//	
	@ResponseBody
	@RequestMapping(value = "/validarBeneficiario", method = RequestMethod.POST)
    public Cuenta validarBeneficiario(@RequestParam String nroCuenta){        
        Cuenta cuentaResult = cuentaService.obtenerDetallesCuenta(nroCuenta);
        return cuentaResult;
    }
	
	@ResponseBody
	@RequestMapping(value = "/uc/validarBeneficiario", method = RequestMethod.POST)
	public Usuario validarUsuarioBeneficiario(@RequestParam String nroCuenta){        
		Usuario usuarioResult = cuentaService.obtenerUsuarioCuenta(nroCuenta);
		return usuarioResult;
	}
	
		
	@RequestMapping(value = "/validarMonto", method = RequestMethod.POST)
	public @ResponseBody Boolean validarMonto(@RequestParam Integer monto, @RequestParam String nroCuenta){	
		
		
		return movimientoService.obtenerMontoValido(nroCuenta, monto);		
	}
	
	@PostMapping("/agregar")
	public String agregarTransferencia(@ModelAttribute("transferencia") Transferencia transferencia) {
		
		
		Boolean transf = movimientoService.insertarTranferencia(transferencia);
		
		return "redirect:/movimientos";		
	}
	
	

	

}
