package com.jtc.transpago.controller.movimiento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jtc.transpago.bean.Cuenta;
import com.jtc.transpago.service.CuentaService;

@RestController
@RequestMapping("/rest/movimientos")
public class RestMovimientoController {

	@Autowired
	private CuentaService cuentaService;
	
	@PostMapping("/validateNroCuenta")
    public @ResponseBody Cuenta validateNroCuentaAjax(@RequestBody Cuenta cuenta) {

        Cuenta cuentaResult = cuentaService.obtenerDetallesCuenta(cuenta.getNroCuenta());
        
        return cuentaResult;
    }

	
}
