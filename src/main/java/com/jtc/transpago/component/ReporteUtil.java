package com.jtc.transpago.component;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class ReporteUtil {

public static JasperPrint generarReporte(String reportNameFile, List dataSource, HashMap paramsReport) throws JRException{
		
		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(dataSource); 
		
		InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(reportNameFile);
		JasperDesign jd = JRXmlLoader.load(is);
		
		JasperReport report = JasperCompileManager.compileReport(jd);
		
		JasperPrint print = JasperFillManager.fillReport(report, paramsReport, ds);
		
		return print;
		
	}

}
