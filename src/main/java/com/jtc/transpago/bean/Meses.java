package com.jtc.transpago.bean;

public enum Meses {
	ENERO("01", "ENERO"),
	FEBRERO("02", "FEBRERO"),
	MARZO("03", "MARZO"),
	ABRIL("04", "ABRIL"),
	MAYO("05", "MAYO"),
	JUNIO("06", "JUNIO"),
	JULIO("07", "JULIO"),
	AGOSTO("08", "AGOSTO"),
	SEPTIEMBRE("09", "SEPTIEMBRE"),
	OCTUBRE("10", "OCTUBRE"),
	NOVIEMBRE("11", "NOVIEMBRE"),
	DICIEMBRE("12", "DICIEMBRE");
	
	private final String displayName;
	private final String displayValue;

	Meses(String displayValue, String displayName) {
        this.displayName = displayName;
        this.displayValue = displayValue;
    }

    public String getDisplayName() {
        return displayName;
    }
    
    public String getDisplayValue() {
    	return displayValue;
    }

}
