/**
 * 
 */

var beneficiario = false;
var monto = false;

$(document).ready(function() {
//	$("#beneficiario").on({
//		change : function() {
//			validarBeneficiario();
//		},
////		focusout : function() {
////			validarBeneficiario();
////		}
//	});
	$("#beneficiario").on("focusout", function(e) {
		validarBeneficiario();
	});
	$("#monto").on({
		change : function() {
			validarMonto();
		},
//		focusout : function() {
//			validarMonto();
//		}
	});
	$("#beneficiario").keyup(function (){
        this.value = (this.value + '').replace(/[^0-9]/g, '');
      });
	$("#monto").keyup(function (){
		this.value = (this.value + '').replace(/[^0-9]/g, '');
	});
	
	
	$("#transf_form").on("submit", function (e){
		if(!beneficiaro || !monto){
			e.preventDefault();
		}
	});

//	$("#transf_form").validate({
//
//		// Specify the validation rules
//		rules : {
//
//			monto : {
//				required : true
//			}
//		// email: {
//		// required: true,
//		// email: true
//		// },
//		// password: {
//		// required: true,
//		// minlength: 5
//		// },
//		// agree: "required"
//		},
//
//		// Specify the validation error messages
//		messages : {
//			beneficiario : "La cuenta del beneficiario es necesaria.",
//			monto : "Entre el monto a transferir"
//		// password: {
//		// required: "Please provide a password",
//		// minlength: "Your password must be at least 5 characters long"
//		// },
//		// email: "Please enter a valid email address",
//		// agree: "Please accept our policy"
//		},
//		onfocusout : false,
//		onkeyup : false,
//		onclick : false,
//
//		submitHandler : function(form) {
//			validarMonto();
//			form.submit();
//		}
//
//	});
	// $("#btn-search-filter").on({
	// submit:function(e){
	// var sel = document.getElementById("btn-search-filter");
	// var valor = sel.options[sel.selectedIndex].value;
	// var texto = sel.options[sel.selectedIndex].text;
	//			
	// alert(valor + texto);
	// e.preventDefault();
	// if ($("#beneficiario").val() == 0) {
	// e.preventDefault();
	// }
	// }
	// });

	// $("#formMov").click(function(e) {
	// var sel = document.getElementById("cuentasUser");
	// var valor = sel.options[sel.selectedIndex].value;
	// var texto = sel.options[sel.selectedIndex].text;
	// e.preventDefault();
	// if (valor == 0) {
	//			
	// seleccionarTodasCuenta();
	// }else{
	// seleccionarCuenta();
	// }
	//	    
	// });
});

function validarBeneficiario() {

	$.ajax({
		type : 'POST',
		url : "http://localhost:8090/movimientos/uc/validarBeneficiario/",
		data : {
			nroCuenta : $("#beneficiario").val()
		},
		success : function(data) {
			if (data != null) {
				var nombre = data.nombre;
				$("#nroCtaBenef").val($("#beneficiario").val());
				$("#nombreBeneficiario").val("");
				$("#nombreBeneficiario").val(nombre);
				beneficiario = true;

			}
		},
		error : function() {
			$("#beneficiario").val("");
			$("#nombreBeneficiario").val("");
			$('#errorBenef').fadeIn('slow', function() {
				$('#errorBenef').delay(5000).fadeOut();
			});
			$("#beneficiario").select();
			beneficiario = false;
		}
	});
}

function validarMonto() {
	$.ajax({
		type : 'POST',
		url : "http://localhost:8090/movimientos/validarBeneficiario/",
		data : {
			nroCuenta : $("#cuentasUser").val()
		},
		success : function(data) {
			var monto = $("#monto").val();
			if (data) {
				if (data.saldo < monto) {
					$("#monto").val("");
					$('#errorMonto').fadeIn('slow', function() {
						$('#errorMonto').delay(5000).fadeOut();
					});
					$("#monto").focus();
					monto = false;
				}else{
					monto = true;
				}
			}
			return;
		},
		error : function() {
			$("#monto").val("");
			$('#errorMonto').fadeIn('slow', function() {
				$('#errorMonto').delay(5000).fadeOut();
			});
			monto = false;
			return false;
		}
	});
}

function seleccionarTodasCuenta() {
	// $('#cuentasUser').addClass("selectCuenta");

	$.ajax({
		type : 'POST',
		url : "http://localhost:8090/movimientos/movimientosAjax",
		data : {
			nroCuenta : $("#cuentasUser").val(),
			mes : $("#mes").val(),
			anno : $("#anno").val()
		},
		success : function(data) {
			return;
		}
	});
}
function seleccionarCuenta() {
	// $('#cuentasUser').addClass("selectCuenta");

	$.ajax({
		type : 'POST',
		url : "http://localhost:8090/movimientos/filter",
		data : $("#transf_form").serialize(),
		success : function(data) {
			return;
		}
	});
}
